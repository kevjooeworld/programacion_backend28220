/* ============================== Tipos de variables ==============================*/ 
// Tipos de variables primitivas
let numVar: number = 0;
let strVar: string = '';
let bolVar: boolean = true;

let numStrvar: number | string = '';
numStrvar = 1;

// Tipos de variables especiales
let undVar: undefined;
let nulVar: null;
let unkVar: unknown;

/* ============================== Estructuras de datos ==============================*/
//Areglos
let arrayNum1 : number[] = [1,2,3];
let arrayNum2 : Array<number> = [1,2,3];
let arrayStr1 : string[] = ['a', 'b', 'c'];

let arrayAny: any[] = [1,'', {numVar}]

//Tuplas
let tuplaNumStr1: (number | string)[] = [1, ''];
let tuplaNumStr2: (number | string) = 1;
tuplaNumStr2 = '1';
// tuplaNumStr2 = true;

// Objetos
let obj1 = {
    numAtrr: 1,
    strAtrr: '1'
}

let numAtrr = obj1.numAtrr;
numAtrr = obj1['numAtrr'];

let obj2 = {
    numAtrr: 1,
    strAtrr: '1',
    objAtrr: {
        numAtrr: 2,
        strAtrr: '2'
    }
}