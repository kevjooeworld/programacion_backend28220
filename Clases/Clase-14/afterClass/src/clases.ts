class ClasePrueba {
    private _atrrNumero: number;
    private _atrrString: string;
    private _atrrBoolean: boolean;
    
    constructor(numAtrr: number, strAtrr: string, bolAtrr: boolean) {
        this._atrrNumero = numAtrr;
        this._atrrString = strAtrr;
        this._atrrBoolean = bolAtrr;
    }

    public get atrrNumero(): number {
        return this._atrrNumero;
    }
    public set atrrNumero(value: number) {
        this._atrrNumero = value;
    }

    public get atrrString(): string {
        return this._atrrString;
    }
    public set atrrString(value: string) {
        this._atrrString = value;
    }

    public get atrrBoolean(): boolean {
        return this._atrrBoolean;
    }
    public set atrrBoolean(value: boolean) {
        this._atrrBoolean = value;
    }
}

class Persona {
    private nombre: string;
    private apellido: string;
    private edad: number; 

    constructor(nombre:string, apellido:string, edad:number) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }
}

// let obj = new ClasePrueba(1, '2', true);

// console.log(obj.atrrNumero);
// obj.atrrNumero = 2;
// console.log(obj.atrrNumero);

export { Persona, ClasePrueba};