"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClasePrueba = exports.Persona = void 0;
var ClasePrueba = /** @class */ (function () {
    function ClasePrueba(numAtrr, strAtrr, bolAtrr) {
        this._atrrNumero = numAtrr;
        this._atrrString = strAtrr;
        this._atrrBoolean = bolAtrr;
    }
    Object.defineProperty(ClasePrueba.prototype, "atrrNumero", {
        get: function () {
            return this._atrrNumero;
        },
        set: function (value) {
            this._atrrNumero = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ClasePrueba.prototype, "atrrString", {
        get: function () {
            return this._atrrString;
        },
        set: function (value) {
            this._atrrString = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ClasePrueba.prototype, "atrrBoolean", {
        get: function () {
            return this._atrrBoolean;
        },
        set: function (value) {
            this._atrrBoolean = value;
        },
        enumerable: false,
        configurable: true
    });
    return ClasePrueba;
}());
exports.ClasePrueba = ClasePrueba;
var Persona = /** @class */ (function () {
    function Persona(nombre, apellido, edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }
    return Persona;
}());
exports.Persona = Persona;
