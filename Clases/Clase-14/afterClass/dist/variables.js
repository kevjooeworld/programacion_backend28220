"use strict";
/* ============================== Tipos de variables ==============================*/
// Tipos de variables primitivas
var numVar = 0;
var strVar = '';
var bolVar = true;
var numStrvar = '';
numStrvar = 1;
// Tipos de variables especiales
var undVar;
var nulVar;
var unkVar;
/* ============================== Estructuras de datos ==============================*/
//Areglos
var arrayNum1 = [1, 2, 3];
var arrayNum2 = [1, 2, 3];
var arrayStr1 = ['a', 'b', 'c'];
var arrayAny = [1, '', { numVar: numVar }];
//Tuplas
var tuplaNumStr1 = [1, ''];
var tuplaNumStr2 = 1;
tuplaNumStr2 = '1';
// tuplaNumStr2 = true;
// Objetos
var obj1 = {
    numAtrr: 1,
    strAtrr: '1'
};
var numAtrr = obj1.numAtrr;
numAtrr = obj1['numAtrr'];
var obj2 = {
    numAtrr: 1,
    strAtrr: '1',
    objAtrr: {
        numAtrr: 2,
        strAtrr: '2'
    }
};
