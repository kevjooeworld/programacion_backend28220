"use strict";
/* ============================== Tipos de retornos ==============================*/
var salida;
//Retorno numerico
function fxNumNum(numPar) {
    return numPar + 1;
}
salida = fxNumNum(1);
console.log(salida);
//Retorno string
var fxStrStr = function (strPar) {
    return '<' + strPar + '>';
};
salida = fxStrStr('Prueba');
console.log(salida);
//Retorno boolean
salida = (function (bolPar) {
    return bolPar;
})(true);
console.log(salida);
//Recibe Objeto
function fxObjObj(objPar) {
    var objRetorno = {
        resultado1: objPar.numAttr + 10,
        resyltado2: objPar['strAtrr'] + '*'
    };
    return objRetorno;
}
salida = fxObjObj({ numAttr: 1, strAtrr: 'test' });
console.log(salida);
/* ============================== Tipos de retornos especiales ==============================*/
//Funcion sin retorno
function myFunction1(numPar1, strPar2, bolPar3) {
    console.log("numPar1: ".concat(numPar1, ", strPar2: ").concat(strPar2, ", bolPar3: ").concat(bolPar3));
}
// funcion que puede retornar any
function myFunction2(numPar1, strPar2, bolPar3) {
    var random = Math.floor(Math.random() * 3 + 1);
    console.log(random);
    switch (random) {
        case 1:
            return numPar1;
        case 2:
            return strPar2;
        case 3:
            return bolPar3;
        default:
            break;
    }
}
console.log(myFunction2(1, '2', true));
