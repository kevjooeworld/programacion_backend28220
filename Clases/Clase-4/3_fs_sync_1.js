const fs = require('fs');

/*=================== [Lectura de archivos]*/

//Directorio nivel inferior
const data1 = fs.readFileSync('./archivos/sync_data1.txt', 'utf-8');
console.log(data1);


/*=================== [Escritura de archivos]*/
fs.writeFileSync('../archivos/sync_data1.txt', 'Hola Coders de todo el mundo!');

/*=================== [Agregar contenido de archivos]*/
fs.appendFileSync('../archivos/sync_data1.txt', ' Es una buena noche para aprender :D');

//Directorio nivel superior
const data2 = fs.readFileSync('../archivos/sync_data1.txt', 'utf-8');
console.log(data2);

let fechaHora = new Date();
let strFechaHora = fechaHora.toString();