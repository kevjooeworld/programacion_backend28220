const fs = require('fs');

fs.promises.readFile('./archivos/package.json', 'utf-8')
.then(contenido => {
    console.log(contenido);
})
.catch(error => {
    console.error(error);
});

(async function leerArchivo() {
    try {
        let contenido = await fs.promises.readFile('./archivos/package.json', 'utf-8');
        console.log(contenido);
    } catch (error) {
        console.error(error);
    }
})();
