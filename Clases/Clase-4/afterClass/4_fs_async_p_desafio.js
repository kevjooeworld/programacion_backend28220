const fs = require('fs');

fs.promises.readFile('./archivos/info.txt', 'utf-8')
.then(contenido => {
    console.log(JSON.parse(contenido));

    //Consigna B
    let info = JSON.parse(contenido);
    console.log(info);

    info.autor = 'Coderhouse';
    //Consigna C
    console.log(info);

    //Consigna D

    fs.promises.writeFile('./archivos/package.json.coder', JSON.stringify(info, null, 2))
    .then(()=>{
        
    })
    .catch((error => {
        console.error(error)
    }));
})
.catch(error => {
    console.error(error);
});
