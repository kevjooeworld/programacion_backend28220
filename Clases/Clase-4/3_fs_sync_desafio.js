const fs = require('fs');

let fechaHoraActual = new Date();
let datosLectura = '';

try {
    fs.writeFileSync('./archivos/fyh.txt', fechaHoraActual.toString(), 'utf-8');
    console.log('Escritura:', fechaHoraActual.toString());
    try {
        datosLectura = fs.readFileSync('./archivos/fyh.txt', 'utf-8');
        console.log('Lectura:', datosLectura);
    } catch (error) {
        throw new Error('No se pudo leer el archivo!');
    }
} catch (error) {
    console.error(error);
    console.log('error al crear archivo');
}
