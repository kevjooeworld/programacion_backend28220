// Muestra las Bases de datos que tienen acceso
show dbs

// Crea base de datos vacia
use clase18

//Crea collecion de forma implicita e inserta un registro
db.createCollection("articulos")

//obtinene info de la Collection
db.articulos.stats() 

//Borra una collection
db.articulos.drop()

//Borra una base de datos
db.dropDatabase()

