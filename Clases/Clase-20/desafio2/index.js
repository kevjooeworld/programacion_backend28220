const admin = require("firebase-admin");

const serviceAccount = require('./db/clase20-c1554-firebase-adminsdk-6rb1f-651a6f7d24.json');

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount)
    });      
} catch (error) {
    
} finally {
    console.log('base Firebase conectada!')
}

Main();

async function Main() {
    try {
        const db = admin.firestore();
        const colores = db.collection('colores');

        const listaColores = [
            {nombre: 'red'},
            {nombre: 'green'},
            {nombre: 'blue'}
        ]

        let id = 1;
        for (const color of listaColores) {
            let doc = colores.doc(`${id}`);
            await doc.create(color);
            id++;
        }
        console.log('Colores agregados!')
        console.log('Un solo doc')
        const snapshot = await colores.where('nombre', '==', 'red').get();
        snapshot.forEach(color => {
            console.log({id: color.id, ...color.data()});
        });

        console.log('Todos los doc')
        const snapshot1 = await colores.get();
        snapshot1.forEach(color => {
            console.log({id: color.id, ...color.data()});
        });

        id = 3;
        const doc = colores.doc(`${id}`);
        let item = await doc.update({nombre: 'navy'});
        console.log("El color ha sido actualziado", item);


        id = 2;
        const doc1 = colores.doc(`${id}`);
        let item1 = await doc1.delete();
        console.log("El usuario ha sido actualziado", item1);

    } catch (error) {
        console.error*(error);
    }
}