const admin = require("firebase-admin");

const serviceAccount = require('./db/clase20-c1554-firebase-adminsdk-6rb1f-651a6f7d24.json');

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount)
    });      
} catch (error) {
    
} finally {
    console.log('base Firebase conectada!')
}

CRUD();

async function CRUD() {
    const db = admin.firestore();
    const usuarios = db.collection('usuarios');

    /* ------------------------------------------------------------------- */
    /*   Escritura de la base de datos collection: usuarios                */
    /* ------------------------------------------------------------------- */
    const listaUsuarios = [
        {nombre: 'Jose', dni: 11223344},
        {nombre: 'Ana', dni: 22334455},
        {nombre: 'Diego', dni: 33445566}
    ]

    //let id = 1;
    for (const usuario of listaUsuarios) {
        // let doc = usuarios.doc(`${id}`); // id manual
        let doc = usuarios.doc(); //id automatico
        await doc.create(usuario);
        //id++;
    }
    console.log('Docs insertados!')

    /* ------------------------------------------------------------------- */
    /*   Lectura de la base de datos collection: usuarios                  */
    /* ------------------------------------------------------------------- */
    const snapshot = await usuarios.get();
    let docs = snapshot.docs;

    const response = docs.map((doc)=>({
        id: doc.id,
        nombre: doc.data().nombre,
        dni: doc.data().dni    
    }))
    console.log(response)

    /* ------------------------------------------------------------------- */
    /*   Actualzacion de la base de datos collection: usuarios                  */
    /* ------------------------------------------------------------------- */

    try {
        let id = 2;
        const doc = usuarios.doc(`${id}`);
        let item = await doc.update({dni: 55443322});
        console.log("El usuario ha sido actualziado", item); 
    } catch (error) {
        console.error(error);
    }

    /* ------------------------------------------------------------------- */
    /*   Delete de la base de datos collection: usuarios                  */
    /* ------------------------------------------------------------------- */
    try {
        let id = 1;
        const doc = usuarios.doc(`${id}`);
        let item = await doc.delete();
        console.log("El usuario ha sido borrado", item); 
    } catch (error) {
        console.error(error);
    }
}