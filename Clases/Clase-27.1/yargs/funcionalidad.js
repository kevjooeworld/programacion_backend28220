import util from "util";

import yargs from "yargs";
const args = yargs(process.argv.slice(2)).argv;

// const yargs = require('yargs/yargs')(process.argv.slice(2));
// const args = yargs.argv;

//obtiene la entrada de argumentos
const args1 = yargs(process.argv.slice(2)).argv;

//imprime la lista de argumentos recibidos
console.log(args);

//"_" Agrupa los argumentos que han sido enviados como valores sin asignar un parametro
console.log('_:', args._);

/*============== [Simulando entradas por CLI] ==============*/
console.log(yargs([1, 2, 3, 4, 5, 6]).argv);
// { _: [ 1, 2, 3, 4, 5, 6 ], '$0': 'funcionalidades.js' }

print(yargs([1, '2', '10.45.23', {a: 1, b: 2}]).argv);
// {_: [ 1, 2, '10.45.23', '[object Object]' ], '$0': 'funcionalidades.js'}

print(yargs([1, '2', '10.45.23','-a', JSON.stringify({a: 1, b: 2})]).argv);
//{_: [ 1, 2, '10.45.23' ], a: '{"a":1,"b":2}', '$0': 'funcionalidades.js'}

let args2 = yargs([1, '2', '10.45.23','-a', JSON.stringify({a: 1, b: 2})]).argv;
console.log(JSON.parse(args2.a))
//{ a: 1, b: 2 }

/*--------- Asignando Parametros como banderas/indicadores */
console.log(yargs(['--bandera1', '--a1',1, 'valor-suelto', '--bandera2']).argv)

/* --------- asignando valores default*/
let args3 = yargs(['valor-sin-param', '--bandera1', '-a', 1,'--nombre', 'Bandadalos'])
            .default({
                nombre: 'Bandalos', 
                apellido: 'Chinos'
            })
            .argv
console.log(args3);

/* --------- asignando alias a los parametros*/
let args4 = yargs(['valor-sin-param', '--bandera1', '-a', 1,'--nom', 'Bandadalos'])
            .alias({a: 'disco', nom: 'nombre'})
            .argv
console.log(args4);

/* --------- utilizando parametros boleanos*/
let args5 = yargs(['valor-sin-param', '--bandera1', '-a', false,'--nom', 'Bandadalos'])
            .boolean(['a', 'nom'])
            .argv
console.log(args5);

//
function print(obj) {
    console.log(util.inspect(obj, {showHidden: false, depth: null, colors: true}))
}