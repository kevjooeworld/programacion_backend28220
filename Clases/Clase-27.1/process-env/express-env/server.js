import express from "express";
import config from "./src/utils/config.js";

const app = express();

app.get('/', (req, res)=>{
    res.send(`Server ok on ${config.NODE_ENV} enviroment!`)
})

const server = app.listen(config.PORT, config.HOST, ()=>{
    console.log(`Servidor escuchando en http://${config.HOST}:${config.PORT}`);
});
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
});