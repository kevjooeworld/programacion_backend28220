import config from "./utils/config.js";

import mongoose from "mongoose";
import UsuarioModel from "./models/Usuarios.model.js";

console.log('process.env.URLBD', config.URLBD);

const URL = config.URLBD;

mongoose.connect(URL)
.then(async ()=>{
    console.log('Base de datos conectada!');

    try {
        /* ------------------------------------------------------------------- */
        /*   Escritura de la base de datos: ecommerce, collection: usuarios    */
        /* ------------------------------------------------------------------- */
        
        const listaUsuarios = [
            { nombre: 'Lucas', apellido: 'Blanco', dni: '30355874' },
            { nombre: 'María', apellido: 'García', dni: '29575148' },
            { nombre: 'Tomas', apellido: 'Sierra', dni: '38654790' },
            { nombre: 'Carlos', apellido: 'Fernández', dni: '26935670' }
        ]

        for (const usuario of listaUsuarios) {
            const obj = new UsuarioModel(usuario);
            await obj.save();
        }

        //----------------------------------------------------------------------------
        /* listar usuarios representándolos en la consola */
        //----------------------------------------------------------------------------
        let usuarios = await UsuarioModel.find({})
        usuarios.forEach(usuario => {
            console.log(JSON.stringify(usuario))
        })
        
    } catch (error) {
        
    }
})
.catch((err)=>{
    console.error(err);
})