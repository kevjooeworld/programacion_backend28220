import dotenv from "dotenv";
dotenv.config();

const config = {
    URLBD: process.env.URLBD
}
export default config;