import minimist from "minimist";

//1 - node desafio-minimist.js 1 2 3 -m dev -p 8080 -d
// { modo: 'dev', puerto: 8080, debug: true, otros: [ 1, 2, 3 ] }
let options1 = {alias: {m: 'modo', p: 'puerto', d:'debug'}}
console.log(minimist(process.argv.slice(2), options1));

//2 - node desafio-minimist.js 1 2 3 
let options2 = {alias: {m: 'modo', p: 'puerto', d:'debug'}, default: {p: 0, m: 'prod', debug: false}}
console.log(minimist(process.argv.slice(2), options2));