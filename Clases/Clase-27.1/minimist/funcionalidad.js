import minimist from "minimist";

const args = minimist(process.argv.slice(2));

console.log(process.argv.slice(2));
console.log(args);

/*============== [Simulando entradas por CLI] ==============*/
console.log(minimist([1, 2, 3, 4, 5, 6]));
// { _: [ 1, 2, 3, 4, 5, 6 ] }

console.log(minimist([1, '2', '10.45.23', {a: 1, b: 2}]));
// { _: [ 1, 2, '10.45.23', { a: 1, b: 2 } ] }

/*--------- Asignando parametros ---------*/
console.log(minimist(['-a', 1, '2','--b1', '10.45.23','--cObjeto', {a: 1, b: 2}]));

/*--------- Asignando Parametros como banderas/indicadores ---------*/
console.log(minimist(['-a', 1,'--b1', '10.45.23','--bandera1']));

//Podemon tener multiples banderas
console.log(minimist(['--bandera1', '-a', 1,'--b1', '10.45.23','--bandera2']));

/*--------- Utilizando options de minimist ---------*/
// Sintaxis del metodo minimist(args[], options)

/* --------- asignando valores default*/
let options = {default: {nombre: 'Bandalos', apellido: 'Chinos'}};
console.log(minimist(['valor-sin-param', '--bandera1', '-a', 1,'--nombre', 'Bandadalos'], options));

/* --------- asignando alias a los parametros*/
options = {alias: {a: 'disco', nom: 'nombre'}};
console.log(minimist(['valor-sin-param', '--bandera1', '-a', 1,'--nom', 'Bandadalos'], options));

