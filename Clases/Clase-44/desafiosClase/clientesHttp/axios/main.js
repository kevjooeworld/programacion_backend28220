import axios from 'axios';
import utils from 'util';

// const graphqlQuery = `
// query {getRecordatorios {titulo,descripcion,timestamp}}
// `

const graphqlQuery = {
  "operationName": "queryConsulta",
  "query": "query queryConsulta{getRecordatorios {titulo,descripcion,timestamp}}",
  "variables": {}
};

let options = {
    url: 'http://localhost:6070/graphql',
    method: 'POST',
    headers: {
        "content-type": "application/json"
    },
    data: graphqlQuery

} 

axios(options)
  .then(response => {
    // Obtenemos los datos
    let result = response.data
    console.log(result);
    console.log(utils.inspect(result, {showHidden: false, depth: 12, colors: true}))
  })
  .catch(error => {
    console.log(error)
  })


/*
  {
    "operationName": "queryConsulta",
    "query": "query queryConsulta{getRecordatorios {titulo,descripcion,timestamp}}",
    "variables": {}
  }
  
*/