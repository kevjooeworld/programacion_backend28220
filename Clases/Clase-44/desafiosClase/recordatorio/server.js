import express from 'express';
import { graphqlHTTP } from 'express-graphql';

import RecordatorioController from './src/controllers/Recordatorio.controller.js';
import RecordatorioSchema from './src/graphql/Recordatorio.schema.js';

const app = express();

app.use(express.static('public'));

app.use('/graphql', graphqlHTTP({
    schema: RecordatorioSchema,
    rootValue: {
        getRecordatorios: RecordatorioController.getRecordatorios,
        createRecordatorio: RecordatorioController.createRecordatorio,
        marcarLeidoRecordatorio: RecordatorioController.marcarLeidoRecordatorio,
        deleteRecordatoriosLeidos: RecordatorioController.deleteRecordatoriosLeidos
    },
    graphiql: true,
}));

const PORT = 6070
app.listen(PORT, () => {
    const msg = `Servidor corriendo en puerto: http://localhost:${PORT}`;
    console.log(msg)
});

/*
    query{
        getRecordatorios {
            titulo,
            descripcion,
            timestamp
        } 
    }

    mutation{
        createRecordatorio(datos: {
            titulo: 'Crear base de datos',
            descripcion: 'EN server MySQL'
        }) {
            
        }
    }

*/