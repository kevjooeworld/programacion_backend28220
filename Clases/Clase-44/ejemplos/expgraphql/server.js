import express from 'express';
import { buildSchema } from 'graphql';
import { graphqlHTTP } from 'express-graphql';

const app = express();

const DB = [
    {
        id: "1",
        name: 'Rick Sanchez',
        category: {
            id: 'A+',
            category: 'Dictador',
            score: '+1'
        }
    },
    {
        id: "2",
        name: 'Morthy Sanchez'
    },
    {
        id: "3",
        name: 'Hella C56'
    }
]


const schema = new buildSchema(`
    type User {
        id: String!,
        name: String,
        category: Category
    }

    type Category {
        idCategory: String!,
        category: String,
        score: String
    }

    type Query {
        findUser(id: String): User
    }
`);

const root = {
    findUser: ({id}) =>{
        const user = DB.find((user)=> user.id === id)
        return user;
    }
}

app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true
}));

app.listen(8080, ()=>{
    console.log('Server en puerto http://localhost:8080')
})

/*
    query {
        findUser(id: "1") {
            name
        }
    }

*/