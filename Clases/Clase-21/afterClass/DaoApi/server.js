import express from "express";
import ProductosDAO from "./src/DAOs/productos.dao.js";

const app = express();

const productoDAO = new ProductosDAO();

app.get('/', (req, res)=>{
    res.status(200).json({msg: 'Ruta / accedida'})
})

app.get('/api/productos/', async (req, res)=>{
    res.status(200).json(await productoDAO.listarAll())
})

const PORT = 8081;
const server = app.listen(PORT, () => {
    console.log(`Servidor escuchando en el puerto ${PORT}`)
});

server.on('error', error => console.log(`Error en servidor: ${error}`))
