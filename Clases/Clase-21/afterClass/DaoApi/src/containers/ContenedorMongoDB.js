import mongoose from 'mongoose';
import config from "../utils/config.js";

const URL = config.mongodb.url;

await mongoose.connect(URL);

class ContenedorMongoDB {
    constructor(nombreColeccion, esquema) {
        this.coleccion = mongoose.model(nombreColeccion, esquema)
    }

    async listarAll(){
        try {
            const docs = await this.coleccion.find({})  
            return docs;
        } catch (error) {
            this.console.log(error);
            return {
                code: '001',
                msg: 'Error al consumir ListarAll()'
            }
        }
    }
}

export default ContenedorMongoDB;

/*
    Testing unitario
*/
// console.log(config.mongodb.url);
// const objContenedorProductos = new ContenedorMongoDB(
//     'productos', 
//     {
//         name: {type: String, require: true},
//         description: {type: String, require: true},
//         price: {type: Number, require: true}
//     }
// )
// console.log(await objContenedorProductos.listarAll());

// const objContenedorUsuarios = new ContenedorMongoDB(
//     'usuarios', 
//     {
//         name: {type: String, require: true},
//         tagname: {type: String, require: true}
//     }
// )
// console.log(await objContenedorUsuarios.listarAll());