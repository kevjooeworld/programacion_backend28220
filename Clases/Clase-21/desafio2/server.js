import  Express from "express";
import { faker } from '@faker-js/faker';

const app = Express();

function generarRandomObjeto() {
    return {
        nombre: faker.name.findName(),
        apellido: faker.name.lastName(),
        color: faker.vehicle.color()
    }
}

app.get('/test', (req, res)=>{
    const objs = [];
    const cant = Number(req.query.cant) || 10;
    
    for (let index = 0; index < cant; index++) {
        objs.push({id: index+1, ...generarRandomObjeto()});
    }
    
    res.json(objs)
});

const PORT = 8080;
const servidor = app.listen(PORT, ()=>{
    console.log(`Servidor Mock escuchando en el puerto: ${PORT}`);
});