import { faker } from '@faker-js/faker';

function generarUsuario() {
    return {
        nombre: faker.name.findName(),
        email: faker.internet.email(),
        website: faker.internet.url(),
        image: faker.image.avatar(),
    }
}

/*
    Testing
    
    console.log(generarUsuario());

*/

export default generarUsuario;