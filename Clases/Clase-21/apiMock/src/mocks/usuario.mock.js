import ContenedorMemoria from "../container/ContenedorMemoria.js";
import generarUsuario from "../utils/generarUsuarios.js";

class UsuariosMock extends ContenedorMemoria {
    constructor () {
        super()
    }

    popular(cant = 50) {
        let listaPopular = [];
        for (let index = 0; index < cant; index++) {
            let nuevoUsuario = generarUsuario();
            listaPopular.push(nuevoUsuario);
        }
        return listaPopular;
    }

    guardarPopular(listaGenerada) {
        
        for (const element of listaGenerada) {
            let newId = 0;
            if (this.elementos.length == 0) {
                newId = 1
            } else {
                newId = this.elementos[this.elementos.length - 1].id + 1
            }
            
            this.elementos.push({id: newId, ...element})
        }
        return this.elementos;
    }
}

/*  
    Testing

    const obj = new UsuariosMock();
    console.log(obj.popular(5));
*/

export default UsuariosMock;