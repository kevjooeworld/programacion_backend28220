import express from "express";
import UsuariosMock from "../mocks/usuario.mock.js";

const usuariosRouter = express.Router();

const objUsuariosMock = new UsuariosMock();

usuariosRouter.get('/popular', (req, res)=>{
    const cant = req.query.cant
    res.json(objUsuariosMock.popular(cant));
});

usuariosRouter.post('/popular', (req, res)=>{
    const cant = req.query.cant
    let listaGenerada = objUsuariosMock.popular(cant);
    res.json(objUsuariosMock.guardarPopular(listaGenerada))
});

usuariosRouter.get('/', (req, res)=>{
    res.json(objUsuariosMock.listarAll());
});

usuariosRouter.post('/', (req, res)=>{
    res.json(objUsuariosMock.guardar(req.body));
});

usuariosRouter.get('/:id', (req, res)=>{
    try {
        let respuesta = objUsuariosMock.listar(req.params.id)
        res.status(200).json(respuesta);
    } catch (error) {
        res.status(404).json({code: 404, msg: error.message});
    }
    
    res.json();
});

export default usuariosRouter;