import Express from "express";

const app = Express(); 

/*==================== Data Mocks ====================*/
const nombres = ['Luis', 'Lucía', 'Juan', 'Augusto', 'Ana']
const apellidos = ['Pieres', 'Cacurri', 'Bezzola', 'Alberca', 'Mei']
const colores = ['rojo', 'verde', 'azul', 'amarillo', 'magenta']

/*==================== ====================*/
function generarRandomItemArray(listaDatos) {
    return listaDatos[Math.floor(listaDatos.length * Math.random())]
}

function generarRandomObjeto() {
    return {
        nombre: generarRandomItemArray(nombres),
        apellido: generarRandomItemArray(apellidos),
        color: generarRandomItemArray(colores)
    }
}

app.get('/test', (req, res)=>{
    let objs = []

    for (let index = 0; index < 10; index++) {
        objs.push(generarRandomObjeto());
    }

    res.json(objs)
})

const PORT = 8080;
const servidor = app.listen(PORT, ()=>{
    console.log(`Servidor Mock escuchando en el puerto: ${PORT}`);
});