import { createTransport } from "nodemailer";
import minimist from "minimist";

const options = {alias: {s: 'subject', h: 'html'}}
const args = minimist(process.argv.slice(2), options)
console.log(args);

const TEST_EMAIL = 'tyrell.hudson74@ethereal.email';
const PASS_EMAIL = '7uaPuHJbHHeNJwUDzN';

const transporter = createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: TEST_EMAIL,
        pass: PASS_EMAIL
    }
});

let emailContent = {
    from: 'Mi primer NodeJS ap <noreply@example.com>',
    to: `"Dear Developer!👩‍💻👨‍💻" <${TEST_EMAIL}>`,
    subject: args.subject || 'sin asunto',
    html: args.html || '<h3 style="color: blue;">Sin contenido <span style="color: green;">Node.js con Nodemailer</span></h3>'
}

try {
    const info = await transporter.sendMail(emailContent);
    console.log(info);
} catch (error) {
    console.error('Hubo un erro:', error);
}