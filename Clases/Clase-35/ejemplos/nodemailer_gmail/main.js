import { createTransport } from "nodemailer";

const TEST_EMAIL = 'correo@gmail.com';
const PASS_EMAIL = 'xxxxxxxxxxxxxxxxx';
const TEST_EMAIL1 = 'xxxxxxxxxx@gmail.com';
const TEST_EMAIL2 = 'xxxxxxxxxx@gmail.com';
const TEST_EMAIL3 = 'xxxxxxxxxxxx@yahoo.com';

const transporter = createTransport({
    service: 'gmail',
    port: 587,
    auth: {
        user: TEST_EMAIL,
        pass: PASS_EMAIL 
    }
});

const emailContent = {
    from: 'NodeJS app <noreply@example.com>',
    to: `"Dear Developer!👩‍💻👨‍💻" ${TEST_EMAIL}, ${TEST_EMAIL1}, ${TEST_EMAIL2}, ${TEST_EMAIL3}`,
    subject: 'Prueba Hola Mundo con Nodemailer en Clase 2do intento ✔',
    text: 'Hello Coders!',
    html: '<h1 style="color: blue;">Contenido de prueba desde <span style="color: green;">Node.js con Nodemailer</span></h1>',
    attachments: [
        {
            path: "./assets/meme.png"
        }
    ]
}

try {
    const info = await transporter.sendMail(emailContent);
    console.log(info);
} catch (error) {
    console.log(error);
}