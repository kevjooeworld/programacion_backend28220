import twilio from 'twilio'

const accountSid = 'ACde948dc61fxxxxxxxxxxxx'
const authToken = '78e0989bdxxxxxxxxxxxxxxx59c'

const client = twilio(accountSid, authToken)

try {
    const numbers = ['+541xxxxxxx8', '+549xxxxxxxx81', '+504xxx60xxx', '+5491xxxxxxxxxxx']

   for (const number of numbers) {
    const message = await client.messages.create({
        body: 'Hola Saludos desde la clase 28220 Node.js 😎!',
        from: '+12xxxxxx20',
        to: number
     })
     console.log(message)
   }
} catch (error) {
   console.log(error)
}
