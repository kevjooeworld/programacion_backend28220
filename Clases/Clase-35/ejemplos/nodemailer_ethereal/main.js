import { createTransport } from "nodemailer";

const TEST_EMAIL = 'tyrell.hudson74@ethereal.email';
const PASS_EMAIL = '7uaPuHJbHHeNJwUDzN';

const transporter = createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: TEST_EMAIL,
        pass: PASS_EMAIL 
    }
});

const emailContent = {
    from: 'Mi primer NodeJS ap <noreply@example.com>',
    to: `"Dear Developer!👩‍💻👨‍💻" <${TEST_EMAIL}>`,
    subject: 'Prueba Hola Mundo con Nodemailer ✔',
    text: 'Hello Coders!',
    html: '<h1 style="color: blue;">Contenido de prueba desde <span style="color: green;">Node.js con Nodemailer</span></h1>'
}

try {
    const info = await transporter.sendMail(emailContent);
    console.log(info);
} catch (error) {
    console.log(error);
}