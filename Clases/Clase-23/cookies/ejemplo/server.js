const express = require('express');
const cookieParser = require('cookie-parser');

const app = express();

app.use(cookieParser('123456789!@#$%^&*()'));

app.get('/', (req, res)=> {
    res.send('Servidor Express Cookies funcionando! +1');
})

app.get('/setCookie', (req, res)=> {
    const cookieName = 'normalCookieNueva';
    const cookieValue = 'valorCookieNormal123'
    res.cookie(cookieName, cookieValue).send(`Cookie ${cookieName} set ${cookieValue}!`);
});

app.get('/setCookieVolatil', (req, res)=> {
    const cookieName = 'normalCookievolatil';
    const cookieValue = 'valorCookieNormal123'
    res.cookie(cookieName, cookieValue, {maxAge: 5000}).send(`Cookie ${cookieName} set ${cookieValue}!`);
});

app.get('/setSignedCookie', (req, res)=> {
    const cookieName = 'normalCookieFirmada';
    const cookieValue = 'valorCookieNormal123'
    res.cookie(cookieName, cookieValue, {signed:true}).send(`Cookie ${cookieName} set ${cookieValue}!`);
});

app.get('/setSignedCookie2', (req, res)=> {
    const cookieName = 'normalCookieFirmada2';
    const cookieValue = 'valorCookieNormal987'
    res.cookie(cookieName, cookieValue, {signed:true}).send(`Cookie ${cookieName} set ${cookieValue}!`);
});

app.get('/getCookies', (req, res)=>{
    res.send({normal: req.cookies, firmadas: req.signedCookies});
})

app.get('/clr', (req, res)=>{
    res.clearCookie('normalCookie').send('Cookie borrada!');
})

const PORT = 8282
app.listen(PORT, () => {
  console.log(`Servidor express escuchando en el puerto ${PORT}`)
})