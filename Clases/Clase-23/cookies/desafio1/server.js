const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const app = express();

app.use(cookieParser('123456789!@#$%^&*()'));
app.use(bodyParser.urlencoded({ extended:true}));

app.get('/cookie', (req, res)=> {
    res.send(req.signedCookies);
})

app.get('/clr', (req, res)=> {
    res.clearCookie('signedCookie2').send('eliminada');
})

app.post('/cookie', (req, res)=> {
    const {nombre, valor, tiempo} = req.body;
    console.log({nombre, valor, tiempo});
    console.log(`nombre: ${nombre}, valor: ${valor}, tiempo: ${tiempo}`);

    if(!nombre || !valor){
        res.json({ error: 'falta nombre ó valor' })
    } 

    if (tiempo) {
        res.cookie(nombre, valor, {signed:true, maxAge: parseInt(tiempo)});
    } else {
        res.cookie(nombre, valor, {signed: true});
    }
    res.json({proceso: 'ok'})
})

const PORT = 8383
app.listen(PORT, () => {
  console.log(`Servidor express escuchando en el puerto ${PORT}`)
})