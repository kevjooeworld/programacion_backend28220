const express = require('express');
const session = require('express-session');

const app = express();

app.use(session({
    secret: '123456789!@#$%^&*()',
    resave: false,
    saveUninitialized: false
}));

function getNombreSession(req) {
    const nombre = req.session.nombre ?? '';
    return nombre;
}

app.get('/', (req, res)=> {
    res.send('Servidor Express Session funcionando! +1');
})

app.get('/con-session', (req, res)=> {
    if (req.session.contador) {
        req.session.contador++;
        res.send(`Hola ${getNombreSession(req)} visitaste la pagina ${req.session.contador} veces.`);
    } else {
        req.session.contador = 1;
        req.session.nombre = req.query.nombre;
        res.send(`Te damos la bienvenida ${getNombreSession(req)}`)
    }
});

app.get('/info', (req, res)=> {
    res.send(req.sessionID);
});

app.get('/olvidar', (req, res)=> {
    req.session.destroy(err=>{
        if (err) {
            res.json({err});
        } else {
            res.send('Hasta luego!');
        }
    });
});

const PORT = 8585
app.listen(PORT, () => {
  console.log(`Servidor express escuchando en el puerto ${PORT}`)
})