const express = require('express');
const session = require('express-session');

const app = express();

app.use(session({
    secret: '123456789!@#$%^&*()',
    resave: false,
    saveUninitialized: false
}));

app.get('/', (req, res)=> {
    res.send('Servidor Express Session funcionando! +1');
})

let contador = 0;
app.get('/sin-session', (req, res)=> {
    res.json({contador: contador++})
})

app.get('/con-session', (req, res)=> {
    if (!req.session.contador) {
        req.session.contador = 1;
        res.send('Bienvenido, primer logeo!');
    } else {
        req.session.contador++;
        res.send(`Ud ha visitado el sitio ${req.session.contador} veces`)
    }
});

app.get('/info', (req, res)=> {
    res.send(req.sessionID);
});

app.get('/logout', (req, res)=> {
    req.session.destroy(err=>{
        if (err) {
            res.json({err});
        } else {
            res.send('Logout ok!');
        }
    });
});

const PORT = 8484
app.listen(PORT, () => {
  console.log(`Servidor express escuchando en el puerto ${PORT}`)
})