/*============================[Modulos]============================*/
import express from "express";
import bodyParser from 'body-parser';
import exphbs from 'express-handlebars';
import path from 'path';

import conf from "./src/utils/serverConfig.js";
import routerRegistro from "./src/routes/registro.routes.js";

const app = express();

/*============================[Middlewares]============================*/

/*----------- Motor de plantillas -----------*/
app.set('views', path.join(path.dirname(''), './src/views'));
app.engine('.hbs', exphbs.engine({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    extname: '.hbs'
}));
app.set('view engine', '.hbs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json())

/*============================[Rutas]============================*/
app.get('/', (req, res)=>{
    res.send('Server ok!!!!')
});

//Ruta de registro
app.use('/api/registro', routerRegistro);

/*============================[Servidor]============================*/
const PORT = conf.port;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
});