import RegistroDAOMongoDB from "../services/RegistroDAOMongoDB.js";

const DAO = new RegistroDAOMongoDB();

const RegistroController = {
    renderRegistro: (req, res) => {
        res.render('registro');
    },

    guardarUsuario: async (req, res) => {
        try {
            let doc = await DAO.guardar(req.body);
            res.render('datos', {datos: {nombre: doc.nombre, direccion: doc.direccion}})
        } catch (error) {
            res.status(500).send(error);
        };
    }
}

export default RegistroController;