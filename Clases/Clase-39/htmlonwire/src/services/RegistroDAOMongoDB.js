import ContenedorMongoDb from "../containers/Contenedor.mongodb.js";
import RegistroModel from "../models/registro.model.js";

class RegistroDAOMongoDB extends ContenedorMongoDb {
    constructor(){
        super(RegistroModel);
    }
}

export default RegistroDAOMongoDB;