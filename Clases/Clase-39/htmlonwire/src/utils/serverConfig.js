import dotenv from 'dotenv';
dotenv.config();

const conf = {
    port: process.env.PORT || 3000 
}

export default conf;