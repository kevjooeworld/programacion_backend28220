export default {
    fileSystem: {
        path: './DB'
    },
    mongodb: {
        host: 'mongodb://localhost:27017/clase38',
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            serverSelectionTimeoutMS: 5000,
        }
    }
}