import express from 'express';
import RegistroController from '../controllers/registro.controller.js';

const routerRegistro = express.Router();

routerRegistro.get('/', RegistroController.renderRegistro);

routerRegistro.post('/', RegistroController.guardarUsuario);


export default routerRegistro;