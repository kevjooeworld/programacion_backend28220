import mongoose from 'mongoose';
import config from '../utils/config.js'

await mongoose.connect(config.mongodb.host, config.mongodb.config);

class ContenedorMongoDb {
    constructor(RegistroModel) {
        this.coleccion = RegistroModel;
    }

    async listarAll() {

        try {
            let docs = await this.coleccion.find({}, { __v: 0 }).lean()
            // console.log(docs);
            return docs;
        } catch (error) {
            return []
        }
    }
    
    async guardar(elemento) {
        try {
            let doc = await this.coleccion.create(elemento);
            return doc;
        } catch (error) {
            throw new Error(`Error al guardar: ${error}`)
        }
    }
}

export default ContenedorMongoDb;