/* ---------------------- Modulos ----------------------*/
const express = require('express');
const path = require('path');
const {Server: HttpServer} = require('http');
const {Server: IOServer} = require('socket.io');

/* ---------------------- Instancia de express ----------------------*/
const app = express();
const httpServer = new HttpServer(app);
const io = new IOServer(httpServer);

/* ---------------------- Middlewares ---------------------- */
app.use(express.static('public'));

/* ---------------------- Motor de plantillas ---------------------- */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

/* ---------------------- WebSocket ---------------------- */
const personas = [
    {nombre: 'Adela', apellido: 'Mtz', edad: 22},
    {nombre: 'Ariel', apellido: 'Ramirez', edad: 23},
    {nombre: 'Astrid', apellido: 'RDZ', edad: 23}
];

io.on('connection', socket => {
    console.log(`Nuevo cliente conectado ${socket.id}`);

    /* Enviar historico de mensajes*/
    socket.emit('mensajes', personas);
});

// Envia los datos almacenados para ser mostrados en el reporte
app.get('/', (req, res)=>{
    res.render('inicio', {personas: personas});
});

/* ---------------------- Servidor ----------------------*/
const PORT = 4334;
const server = httpServer.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
});

/*
    npm init -y 
    npm install express socket.io
        "dev": "nodemon server.js"

*/