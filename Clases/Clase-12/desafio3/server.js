/* ---------------------- Modulos ----------------------*/
const express = require('express');
const {Server: HttpServer} = require('http');
const {Server: IOServer} = require('socket.io');

/* ---------------------- Instancia de express ----------------------*/
const app = express();
const httpServer = new HttpServer(app);
const io = new IOServer(httpServer);

/* ---------------------- Middlewares ---------------------- */
app.use(express.static('public'));

/* ---------------------- WebSocket ---------------------- */
const mensajes = [];

io.on('connection', socket => {
    console.log(`Nuevo cliente conectado ${socket.id}`);

    /* Enviar historico de mensajes*/
    socket.emit('mensajes', mensajes);

    /* Escuchar los nuevos mensajes*/
    socket.on('mensajeNuevo', data=>{
        mensajes.push(data);
        /* Se actualiza la vista*/
        io.sockets.emit('mensajes', mensajes);
    });
});

/* ---------------------- Servidor ----------------------*/
const PORT = 4334;
const server = httpServer.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
});

/*
    npm init -y 
    npm install express socket.io
        "dev": "nodemon server.js"

*/