import Koa from 'Koa';
import koaBody from 'koa-body';

const app = new Koa();
app.use(koaBody());

import booksRouter from './src/routes/books.routes.js';

app.use(booksRouter.routes());

app.use(async ctx => {
    ctx.body = 'Hola Coders!'
})

app.listen(3000, ()=>{
    console.log('App running http://localhost:3000')
});