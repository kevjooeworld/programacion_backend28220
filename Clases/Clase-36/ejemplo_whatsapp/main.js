import twilio from "twilio";
import dotenv from 'dotenv';
dotenv.config();

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;

const client = twilio(accountSid, authToken);

// const numbers = ['+5491131467302', '+50499605855', '+5491149471981'];
const numbers = ['+50499605855' ];

try {
    let message = '';

    for (const number of numbers) {
        message = await client.messages.create({
            from: 'whatsapp:+14155238886',
            to: `whatsapp:${number}`,
            body: 'Todo funciona bien con Twilio y NodeJS!'
        });

        console.log(message);
    }
} catch (error) {
    console.error(error);
}