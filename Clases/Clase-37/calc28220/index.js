function sumar(parm1, parm2) {
    try {
        return Number(parm1) + Number(parm2);        
    } catch (error) {
        return -1
    }
}

function restar(parm1, parm2) {
    try {
        return Number(parm1) - Number(parm2);        
    } catch (error) {
        return -1
    }
}

function multiplicar(parm1, parm2) {
    try {
        return Number(parm1) * Number(parm2);        
    } catch (error) {
        return -1
    }
}

function dividir(parm1, parm2) {
    try {
        return Number(parm1) / Number(parm2);        
    } catch (error) {
        return -1
    }
}

module.exports = {
    sumar, restar, multiplicar, dividir
}