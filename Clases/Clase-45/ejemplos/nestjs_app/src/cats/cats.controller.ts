import { Body, Controller, Post, Get } from '@nestjs/common';
import { CatsService } from './cats.service';
import { CreateCatDto } from 'src/dto/create-cat.dto';
import { Cat } from '../interfaces/cat.interface';

@Controller('cats-route')
export class CatsController {
    constructor(private readonly catsService: CatsService) { }

    @Post()
    async create(@Body() createCatDto: CreateCatDto): Promise<Cat> {
        this.catsService.create(createCatDto);
        return createCatDto;
    }

    @Get()
    async findAll(): Promise<Cat[]> {
        return this.catsService.findAll();
    }
}
