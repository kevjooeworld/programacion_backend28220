import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  /* Config Swagger*/
  const options = new DocumentBuilder()
    .setTitle('Ejemplo Gatos')
    .setDescription('Proyecto para la clase 45')
    .setVersion('1.0.0')
    .addTag('cats')
    .build()
  
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document); 

  await app.listen(3000, ()=> {
    console.log('App ejecutando en http://localhost:3000/')
  });
}
bootstrap();
