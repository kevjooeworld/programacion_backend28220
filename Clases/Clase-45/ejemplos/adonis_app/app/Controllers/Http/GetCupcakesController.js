const cupcakeModel = use('App/Models/Cupcake');

class GetCupcakesController {
    async index({response, view}) {
        const cupcakesList = (await cupcakeModel.all()).toJSON();
        return view.render('cupcakes', {cupcakesList})
    }
}

module.exports = GetCupcakesController;