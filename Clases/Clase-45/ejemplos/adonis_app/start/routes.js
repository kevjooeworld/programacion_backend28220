'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

const cupcakeModel = use('App/Models/Cupcake')
// const cupcakeController = use('App/Controllers/Http/GetCupcakesController')

Route.on('/').render('welcome')

Route.get('/hola', ()=> 'Hola Mundo! Somos Coders 😎')

Route.get('/cupcakes-sin-controlador', async ({view})=> {
    const cupcakesList = (await cupcakeModel.all()).toJSON();
    return view.render('cupcakes', {cupcakesList})
})

Route.get('/cupcakes-con-controlador', 'GetCupcakesController.index');
