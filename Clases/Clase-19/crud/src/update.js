const Producto = require('./models/Product');
const mongoose = require('mongoose');

const URL = 'mongodb://localhost:27017/ecommerce';

mongoose.connect(URL)
.then(async ()=>{
    try {
        let resultado = await Producto.updateOne({_id: '622018c314e62d921f8614a2'}, {$set: {name:'Mouse', description: 'Corsair Harpoon RGB Wireless', price: 60.5}})
        console.log(resultado);
    } catch (error) {
        console.err(`Error: ${error}`);
    } finally {
        mongoose.disconnect().catch((err)=>{
            console.error(err);
        })
    }
})
.catch((err)=>{
    console.error('Error al conectarse a la Base de datos')
});