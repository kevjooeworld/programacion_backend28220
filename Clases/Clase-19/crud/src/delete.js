const Producto = require('./models/Product');
const mongoose = require('mongoose');

const URL = 'mongodb://localhost:27017/ecommerce';

mongoose.connect(URL)
.then(async ()=>{
    try {
        
        let resultado = await Producto.deleteOne({_id: '622018ce157ddf79facbe9eb'})
        console.log(resultado);

    } catch (error) {
        console.err(`Error: ${error}`);
    } finally {
        mongoose.disconnect().catch((err)=>{
            console.error(err);
        })
    }
})
.catch((err)=>{
    console.error('Error al conectarse a la Base de datos')
});