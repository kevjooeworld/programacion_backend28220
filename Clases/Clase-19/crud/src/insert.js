const Producto = require('./models/Product');
const mongoose = require('mongoose');

const URL = 'mongodb://localhost:27017/ecommerce';

mongoose.connect(URL)
.then(async ()=>{
    try {
        const prod1 = new Producto({
            name: 'Laptop',
            description: 'Razer Blade 14 inch',
            price: 1200
        })

        const prod2 = new Producto({
            name: 'Teclado',
            description: 'Keychron K2 V2 (En)',
            price: 400.99
        })

        let doc = await prod2.save();
        console.log(doc);
        
    } catch (error) {
        console.err(`Error: ${error}`);
    } finally {
        mongoose.disconnect().catch((err)=>{
            console.error(err);
        })
    }
})
.catch((err)=>{
    console.error('Error al conectarse a la Base de datos')
});