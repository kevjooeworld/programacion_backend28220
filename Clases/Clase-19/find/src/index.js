const UsuariosModelo = require('./models/Usuario.model');
const mongoose = require('mongoose');

const URL = 'mongodb://localhost:27017/ecommerce';

async function cargarUsuarios(listaUsuarios) {
    let resultados = [];
    try {
        for (const usuario of listaUsuarios) {
            let obj = new UsuariosModelo(usuario);
            resultados.push(await obj.save());
        }
    } catch (error) {
        console.error(error);
    }
    return resultados;
}

// Projection + Filters
async function listarProjectionFilter(filtros, campos) {
    let resultado = [];

    try {
        resultado = await UsuariosModelo.find(filtros, campos);
        // console.log(resultado);
    } catch (error) {
        console.error(error);
    }

    return resultado;
}

//Sort
async function listarProjectionSort(filtros, campos, order) {
    let resultado = [];

    try {
        resultado = await UsuariosModelo.find(filtros, campos).sort(order);
        // console.log(resultado);
    } catch (error) {
        console.error(error);
    }

    return resultado;
}

mongoose.connect(URL)
.then(async ()=>{
    try {
        let resultados = [];

        console.log('1- Usuarios eliminados')
        resultados = await UsuariosModelo.deleteMany({});
        console.log(resultados);

        console.log('1- Usuarios cargados')
        const listaUsuarios = [
            {nombre: 'Juan', apellido: 'Perez', email: 'jp@g.com', contrasenia: '123456'},
            {nombre: 'Pedro', apellido: 'Suarez', email: 'ps@g.com', contrasenia: '123456'},
            {nombre: 'Ana', apellido: 'Mei', email: 'am@g.com', contrasenia: '123456'},
            {nombre: 'Mirta', apellido: 'Blanco', email: 'mb@g.com', contrasenia: '123456'}
        ];
        resultados = await cargarUsuarios(listaUsuarios);
        console.log(resultados);

        console.log('2- projection')
        let lista1 = await listarProjectionFilter({nombre: 'Juan'}, {nombre: 1, apellido: 1, email: 1, _id: 0});    
        console.log(lista1);

        console.log('3- Sort')
        let lista2 = await listarProjectionSort({}, {nombre: 1, apellido: 1, _id: 0}, {nombre: -1});    
        console.log(lista2);

        console.log('4 - Skip')
        let lista3 = await UsuariosModelo.find({}, {nombre: 1, apellido: 1, _id: 0}).sort({nombre: -1}).skip(1);
        console.log(lista3);

        console.log('4 - Limit')
        let lista4 = await UsuariosModelo.find({}, {nombre: 1, apellido: 1, _id: 0}).sort({nombre: -1}).limit(1);
        console.log(lista4);


    } catch (error) {
        console.err(`Error: ${error}`);
    } finally {
        mongoose.disconnect().catch((err)=>{
            console.error(err);
        })
    }
})
.catch((err)=>{
    console.error('Error al conectarse a la Base de datos')
});