const { Schema, model } = require('mongoose');

const usuarioSchema = new Schema({
    nombre: String,
    apellido: String,
    email: {
        type:String,
        required: true,
        unique: true
    },
    contrasenia: String
});

const UsuariosModelo = model('usuarios', usuarioSchema);

module.exports = UsuariosModelo;