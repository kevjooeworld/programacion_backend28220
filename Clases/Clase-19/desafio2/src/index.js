const EstudiantesModel = require('./models/Estudiantes.model');
const mongoose = require('mongoose');

const URL = 'mongodb://localhost:27017/colegio';

mongoose.connect(URL)
.then((db)=>{
    console.log('\nBase de datos conectada');

    console.log('\n1) Estudiantes ordenados por orden alfabético según sus nombres')
    EstudiantesModel.find({}).sort({nombre: 1})
    .then((resultado)=>{
        
        resultado.forEach(element => {
            console.log(element);
        });

        console.log('\n2) El estudiante más joven')
        return EstudiantesModel.find({}).sort({edad: 1}).limit(1);
    })
    .then(resultado=>{
        resultado.forEach(element => {
            console.log(element);
        });
    })
    .catch(err => { throw new Error(`Error en lectura ${err}`) })
    .finally(()=>{
        mongoose.disconnect().catch(err => { 
            throw new Error('error al desconectar la base de datos') 
        })
    })
})
.catch((err)=>{
    console.error('Error al conectarse a la base de datos');
})