const minimist = require('minimist');

process.on('exit', (code)=>{
    let infoErr = {}

    console.log('exit code',code)

    if (code == 5) {
        infoErr = {
            descripcion: 'error de tipo',
            numeros: numeros,
            tipos: tipos
        }
        
    } else {
        if (code == -4) {
            infoErr = {
                descripcion: 'Entrada vacia'
            }
        }
    }
    console.log(infoErr)
})

const args = minimist(process.argv.slice(2));
// const numeros = Object.values(args)[0]
const numeros = args._;

if (numeros.length == 0) {
    process.exit(-4)
}

console.log(numeros);

const tipos = tipo(numeros);
console.log('tipos', tipos)

validacionTipo(tipos);

const suma = numeros.reduce((a, b)=>{
    let acumulador = 0;
    acumulador = parseInt(a) + parseInt(b);
    return acumulador;
})

const info = {
    datos: {
        numeros: numeros,
        promedio: suma/numeros.length,
        max: Math.max.apply(null, numeros),
        min: Math.min.apply(null, numeros),
        ejecutable: process.title,
        pid: process.pid
    }
}
console.log(info);

function tipo(array) {
    let arrayTipes = [];

    array.forEach(element => {
        arrayTipes.push(typeof(element));
    });

    return arrayTipes;
}

function validacionTipo(array) {
    array.forEach(element => {
        if (element != 'number') {
            console.log(element, 'number')
            process.exit(5)
        }
    });
}

