const util = require('util');

console.log(`Directorio actual de trabajo: ${process.cwd()}`);
console.log(`Id del proceso: ${process.pid}`);
console.log(`Version de NodeJS: ${process.version}`);
console.log(`Nombre del proceso: ${process.title}`);
console.log(`Sistema Operativo: ${process.platform}`);
console.log(`Uso de la memoria: ${process.memoryUsage()}`);
print(process.memoryUsage());

function print(obj) {
    console.log(util.inspect(obj, {showHidden: false, depth: 12, colors: true}));
}