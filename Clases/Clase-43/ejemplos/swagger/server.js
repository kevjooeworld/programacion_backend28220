import express, { json } from 'express';
import swaggerJsDoc from 'swagger-jsdoc';
import swaggerUI from 'swagger-ui-express';

const numeros = []

const app = express()
app.use(json())

const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Express API Operaciones - Coder House",
            description: "API Operaciones docuemntado para la clase",
        },
    },
    apis: [ './docs/**/*.yaml' ],
};

const swaggerSpecs = swaggerJsDoc(options);

app.use('/api/docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs))

app.get('/', (req, res) => {
    res.send('Ruta base API Documentada')
})


app.post('/operaciones', (req, res) => {
    const { numero } = req.body
    numeros.push(numero)
    res.send(`Número ${numero} almacenado`)
})

app.get('/operaciones', (req, res) => {
    res.json({ numeros })
})

const PORT = 4343
const server = app.listen(PORT,
    () => console.log(`Servidor http escuchando en el puerto ${server.address().port}`)
)
server.on('error', error => console.log(`Error en servidor http`, error))
