/* ---------------------- Modulos ----------------------*/
const express = require('express');
const path = require('path');
const {Server: HttpServer } = require('http');
const {Server: IOServer } = require('socket.io');
/* ---------------------- Instancia de express ----------------------*/
const app = express();
const httpServer = new HttpServer(app);
const io = new IOServer(httpServer);

/* ---------------------- Middlewares ---------------------- */
app.use(express.static('public'));
app.use(express.json())

/* ---------------------- Montor de plantillas ---------------------- */
app.set('views', path.join('views'));
app.set('view engine', '');

/* ---------------------- Socket ----------------------*/
io.on('connection', (socket) =>{
    console.log(`Nuevo cliente conectado! >${socket.id}`);
});

/* ---------------------- Servidor ----------------------*/
const PORT = 7272;
const server = httpServer.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
});

/*
    npm init -y 
    npm install express body-parser express-handlebars
        "dev": "nodemon server.js"

*/