/* ---------------------- Modulos ----------------------*/
const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
/* ---------------------- Instancia de express ----------------------*/
const app = express();

/* ---------------------- Middlewares ---------------------- */
app.use(express.static('public'));
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }));

/* ---------------------- Conf. Motor de plantillas ---------------------- */
app.engine('cte', async (filePath, options, callback)=>{
    try {
        const contenido = await fs.promises.readFile(filePath);
        const htmlGenerado = contenido.toString()
                                    .replace('^^nombre$$', options.nombre)
                                    .replace('^^apellido$$', options.apellido)
                                    .replace('^^edad$$', options.edad)
                                    .replace('^^email$$', options.email)
                                    .replace('^^teléfono$$', options.telefono);

        callback(null, htmlGenerado);
    } catch (error) {
        callback(new Error(error), null);
    }
});

/* ---------------------- Ruta de plantillas*/
app.set('views', './views');
/* ---------------------- conf extension*/
app.set('view engine', 'cte');

/* ---------------------- Rutas ----------------------*/
app.get('/cte1', (req, res)=>{
    const datos = {
        nombre: 'Leonardo',
        apellido: 'Massimino',
        edad: 28,
        email: 'leomass@gmail.com',
        telefono: '+777 889955'
    }    
    
    res.render('plantilla', datos);
});

app.get('/cte2', (req, res)=>{
    const datos = {
        nombre: 'Alejandra',
        apellido: 'Martinez',
        edad: 29,
        email: 'alemtz@gmail.com',
        telefono: '+775 779955'
    }   
    res.render('plantilla', datos);
});

/* ---------------------- Servidor ----------------------*/
const PORT = 7272;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
});

/*
    npm init -y 
    npm install express body-parser express-handlebars
        "dev": "nodemon server.js"

*/