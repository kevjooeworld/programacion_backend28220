/* ---------------------- Modulos ----------------------*/
const express = require('express');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const path = require('path');
/* ---------------------- Instancia de express ----------------------*/
const app = express();

/* ---------------------- Middlewares ---------------------- */
app.use(express.static('public'));
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }));

/* ---------------------- Conf Motor ----------------------*/
app.set('views', path.join(__dirname, 'views'));

app.engine('hbs', exphbs.engine({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: 'hbs'
}));

app.set('view engine', 'hbs');

/* ---------------------- Rutas ----------------------*/
app.get('/datos', (req, res)=>{
    const datos = {
        nombre: 'Alejandra',
        apellido: 'Martinez',
        edad: 29,
        email: 'alemtz@gmail.com',
        telefono: '+775 779955'
    }   
    res.render('plantilla', datos);
});

app.get('/datos2', (req, res)=>{
    const datos = {
        nombre: 'Maricel',
        apellido: 'Ochoa',
        edad: 26,
        email: 'mariocho@gmail.com',
        telefono: '+775 889955'
    }   
    res.render('plantilla', datos);
});

app.get('/ejemploPartials', (req, res)=>{
    res.send('ruta ejemplo get ');
});

/* ---------------------- Servidor ----------------------*/
const PORT = 7272;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
});

/*
    npm init -y 
    npm install express body-parser express-handlebars
        "dev": "nodemon server.js"

*/