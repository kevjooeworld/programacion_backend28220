/*
    http://localhost:8080/frase/Hola%20servidor%20Deno!

    http://localhost:8080/frase2?frase=Hola%20servidor%20Deno!


*/

import { Application, Router, Context, helpers } from "https://deno.land/x/oak@v10.6.0/mod.ts";

const router = new Router();

router.get("/frase/:frase", (ctx: Context) => {
    const { frase } = helpers.getQuery(ctx,  { mergeParams: true });

    console.log(helpers.getQuery(ctx,  { mergeParams: true }))
    
    const fraseReverse = frase.split(' ').reverse().join(' ');
    ctx.response.body = `<!DOCTYPE html>
      <html>
        <head><title>Hello oak!</title><head>
        <body>
          <h1>${fraseReverse}</h1>
        </body>
      </html>
    `;
})

router.get("/frase2", (ctx: Context) => {
    const {frase} = helpers.getQuery(ctx,  { mergeParams: true });
    console.log(helpers.getQuery(ctx,  { mergeParams: true }))

    const fraseReverse = frase.split(' ').reverse().join(' ');
    ctx.response.body = `<!DOCTYPE html>
      <html>
        <head><title>Hello oak!</title><head>
        <body>
          <h1>${fraseReverse}</h1>
        </body>
      </html>
    `;
})

const app = new Application();
app.use(router.routes());
app.use(router.allowedMethods());

app.listen({ port: 8080 });