const { options } = require('./utils/options');
const knex = require('knex')(options);

const listaAutos = [
    {marca: 'TOYOTA', modelo: 'RUSH'}
]

knex('autos').insert(listaAutos)
    .then(()=>{
        console.log('Registros insertados');
    })
    .catch((error)=>{
        console.error(
            {
                codigo: `${error.errno}|${error.code}`,
                msg: error.sqlMessage
            }
        )
    })
    .finally(()=>{
        knex.destroy();
    });