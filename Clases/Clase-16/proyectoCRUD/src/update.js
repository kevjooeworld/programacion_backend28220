const { options } = require('./utils/options');
const knex = require('knex')(options);


knex.from('autos').where('id', '5').update({modelo: 'SORENTO'})
    .then(()=>{
        console.log('Registro 5 actualizado!')
    })
    .catch((error)=>{
        console.error(
            {
                codigo: `${error.errno}|${error.code}`,
                msg: error.sqlMessage
            }
        )
    })
    .finally(()=>{
        knex.destroy();
    });