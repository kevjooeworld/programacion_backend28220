/*
-- Query: SELECT * FROM db_test.autos
LIMIT 0, 1000

-- Date: 2022-02-21 19:40
*/
INSERT INTO `` (`id`,`marca`,`modelo`) VALUES (1,'BMW','528i');
INSERT INTO `` (`id`,`marca`,`modelo`) VALUES (2,'HONDA','CRV');
INSERT INTO `` (`id`,`marca`,`modelo`) VALUES (3,'TOYOTA','RAV-4');
INSERT INTO `` (`id`,`marca`,`modelo`) VALUES (4,'KIA','PICANTO');
INSERT INTO `` (`id`,`marca`,`modelo`) VALUES (5,'KIA','SORENTO');
INSERT INTO `` (`id`,`marca`,`modelo`) VALUES (6,'TOYOTA','RUSH');
