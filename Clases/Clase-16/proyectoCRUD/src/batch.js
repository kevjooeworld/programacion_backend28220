const { options } = require('./utils/options');
const knex = require('knex')(options);

const listaAutos = [
    {marca: 'BMW', modelo: '528i'},
    {marca: 'HONDA', modelo: 'CRV'},
    {marca: 'TOYOTA', modelo: 'RAV-4'},
    {marca: 'TOYOTA', modelo: 'RUSH'},
]

const batch = async ()=>{
    try {
        console.log('1- Limpieza de tabla')
        await knex.from('autos').del();

        console.log('2- Insertar registros')
        await knex('autos').insert(listaAutos);

        console.log('3- Leer registros')
        let respuesta = await knex.from('autos').select('*');
        console.table(respuesta);

    } catch (error) {
        console.error(
            {
                codigo: `${error.errno}|${error.code}`,
                msg: error.sqlMessage
            }
        )
    } finally {
        knex.destroy();
    }
}

batch();