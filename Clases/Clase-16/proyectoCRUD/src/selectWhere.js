const { options } = require('./utils/options');
const knex = require('knex')(options);

// knex.from('autos').select('id', 'marca')
knex.from('autos').select('*').where('marca', '=', 'TOYOTA').andWhere('modelo', '=', 'RUSH')
    .then((rows)=>{
        for (const row of rows) {
            console.log(`${row['id']}, ${row['marca']}, ${row['modelo']}`);
        }
        console.table(rows)
    })
    .catch((error)=>{
        console.error(
            {
                codigo: `${error.errno}|${error.code}`,
                msg: error.sqlMessage
            }
        )
    })
    .finally(()=>{
        knex.destroy();
    });