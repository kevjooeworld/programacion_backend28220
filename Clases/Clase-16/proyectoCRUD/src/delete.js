const { options } = require('./utils/options');
const knex = require('knex')(options);

knex.from('autos').del()
    .then(()=>{
        console.log('Registros eliminados')
    })
    .catch((error)=>{
        console.error(
            {
                codigo: `${error.errno}|${error.code}`,
                msg: error.sqlMessage
            }
        )
    })
    .finally(()=>{
        knex.destroy();
    });