
//Tradicional
function tradicional(texto1, texto2) {
    let textoConcatendo = `${texto1} ${texto2}`;
    return textoConcatendo;
}

//Asignada
const fxTradicional = function (texto1, texto2) {
    let textoConcatendo = `${texto1} ${texto2}`;
    return textoConcatendo;
}

//Arrow function
const fxArrowfunction = (texto1, texto2) => {
    let textoConcatendo = `${texto1} ${texto2}`;
    return textoConcatendo;
}

const fxArrowfunction2 = texto1 => {
    let textoConcatendo = `${texto1} Mundo!`;
    return textoConcatendo;
}

const fxArrowfunction3 = texto1 => `${texto1} Mundo!`;

const fxDuplicidad = numero => numero*2;

//console.log(tradicional('Hola', 'Mundo!'));
//console.log(fxTradicional('Hola', 'Mundo!'));
//console.log(fxArrowfunction('Hola', 'Mundo!'));
//console.log(fxArrowfunction2('Flecha Hola'));
//console.log(fxArrowfunction3('Arrow '));
console.log(fxDuplicidad(4));


