function crearControladorAsignarMemoria (objMemoria) {
    return (cantidadAsignar)=>{
        if ((objMemoria.memoriaUso + cantidadAsignar) > objMemoria.capacidadMB) {
            return {objMemoria, estado: false, mensaje:'Memoria insuficiente'};
        } else {
            objMemoria.memoriaUso += cantidadAsignar;
            return {objMemoria, estado: true, mensaje:'Memoria asignada satisfactoriamente'};
        }
    }
}

function crearControladorLiberarMemoria (objMemoria) {
    return (cantidadLiberar)=>{
        if (cantidadLiberar > objMemoria.memoriaUso) {
            return {objMemoria, estado: false, mensaje:'Memoria no liberada'};
        } else {
            objMemoria.memoriaUso -= cantidadLiberar;
            return {objMemoria, estado: true, mensaje:'Memoria liberada satisfactoriamente'};
        }
    }
}

//haciendo uso del codigo
const ADATA128 = {
    idRegistro: 'USA128FTHUOP',
    tipo: 'SSD',
    capacidadMB: 131072,
    memoriaUso: 0
}

const ADATA256 = {
    idRegistro: 'USA256PGSNHF',
    tipo: 'SSD',
    capacidadMB: 262144,
    memoriaUso: 0
}

const ASIGNAR_ADATA128 = crearControladorAsignarMemoria(ADATA128);
const ASIGNAR_ADATA256 = crearControladorAsignarMemoria(ADATA256);
const LIBERAR_ADATA128 = crearControladorLiberarMemoria(ADATA128);
const LIBERAR_ADATA256 = crearControladorLiberarMemoria(ADATA256);

console.log(ASIGNAR_ADATA128(204800));
console.log(ASIGNAR_ADATA256(204800));
console.log(LIBERAR_ADATA256(102400));