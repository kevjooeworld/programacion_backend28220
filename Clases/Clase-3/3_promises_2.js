new Promise((resolve, reject) => {
    setTimeout(() => resolve(1), 1000);
})
    .then(result => {
        console.log(`then 1: Se recibe: ${result}`);
        let nuevoValor = result * 2;
        console.log(`then 1: Se envia: ${nuevoValor}`);
        return nuevoValor;
    })
    .then(result => {
        console.log(`then 2: Se recibe: ${result}`);
        let nuevoValor = result * 3;
        console.log(`then 2: Se envia: ${nuevoValor}`);
        return nuevoValor;
    })
    .then(result => {
        console.log(`then 3: Se recibe: ${result}`);
        let nuevoValor = result + 10;
        console.log(`then 3: Se envia: ${nuevoValor}`);
        return nuevoValor;
    });


    