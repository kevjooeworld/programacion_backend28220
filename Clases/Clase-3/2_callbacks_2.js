function operacionesMemoria(objMemoria, cantidad, callback) {
    let respuesta = callback(objMemoria, cantidad)
    
    let informe = {
        idRegistro: respuesta.objMemoria.idRegistro,
        tipo: respuesta.objMemoria.tipo,
        capacidadMB: respuesta.objMemoria.capacidadMB,
        memoriaUso: respuesta.objMemoria.memoriaUso,
        hora: new Date(),
        informeMsg : respuesta.mensaje
    } 
    
    console.log(informe);
}

const asignarMemoria = (objMemoria, cantidadAsignar) => {
    if ((objMemoria.memoriaUso + cantidadAsignar) > objMemoria.capacidadMB) {
        return {objMemoria, estado: false, mensaje:'Memoria insuficiente'};
    } else {
        objMemoria.memoriaUso += cantidadAsignar;
        return {objMemoria, estado: true, mensaje:'Memoria asignada satisfactoriamente'};
    }
}

const liberarMemoria = (objMemoria, cantidadLiberar) => {
    if (cantidadLiberar > objMemoria.memoriaUso) {
        return {objMemoria, estado: false, mensaje:'Memoria no liberada'};
    } else {
        objMemoria.memoriaUso -= cantidadLiberar;
        return {objMemoria, estado: true, mensaje:'Memoria liberada satisfactoriamente'};
    }
}

//haciendo uso del codigo
const ADATA128 = {
    idRegistro: 'USA128FTHUOP',
    tipo: 'SSD',
    capacidadMB: 131072,
    memoriaUso: 0
}

const ADATA256 = {
    idRegistro: 'USA256PGSNHF',
    tipo: 'SSD',
    capacidadMB: 262144,
    memoriaUso: 0
}

operacionesMemoria(ADATA128, 204800, asignarMemoria);
operacionesMemoria(ADATA256, 204800, asignarMemoria);
operacionesMemoria(ADATA256, 102400, liberarMemoria);