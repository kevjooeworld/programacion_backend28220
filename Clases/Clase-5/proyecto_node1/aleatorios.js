listaOcurrencias = {};

function generarAleatorio(numInicial, numFinal) {
    let int = parseInt(Math.random() * numFinal) + numInicial;
    return int;
}

function generarConteoAleatorios(numInicial, numFinal, iteraciones) {
    let clave = 0;
    for (let index = 0; index < iteraciones; index++) {
        clave = generarAleatorio(numInicial, numFinal);
        if (listaOcurrencias.hasOwnProperty(clave.toString())) {
        //if (clave.toString() in listaOcurrencias) {
            listaOcurrencias[clave.toString()] += 1;    
        } else {
            listaOcurrencias[clave.toString()] = 1;
        }
    }
    return listaOcurrencias;
}

module.exports = generarConteoAleatorios;