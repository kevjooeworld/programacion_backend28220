const _ = require('lodash'); 

const productos = [
    { id:1, nombre:'Escuadra', precio:323.45 },
    { id:2, nombre:'Calculadora', precio:234.56 },
    { id:3, nombre:'Globo Terráqueo', precio:45.67 },
    { id:4, nombre:'Paleta Pintura', precio:456.78 },
    { id:5, nombre:'Reloj', precio:67.89 },
    { id:6, nombre:'Agenda', precio:78.90 }
]

function obtenerNombre(listaProductos) {
    let nombres = '';
    for (const producto of listaProductos) {
        nombres += producto.nombre + ', ';
    }
    return nombres;
}

function calcularPrecioTotal(listaProductos) {
    let precioTotal = 0;
    for (const producto of listaProductos) {
        precioTotal += producto.precio;
    }
    //return Number(precioTotal.toFixed(2));
    return parseFloat(precioTotal.toFixed(2));
}

function calcularPrecioPromedio(listaProductos) {
    let precioProm = calcularPrecioTotal(productos)/listaProductos.length;
    
    return parseFloat(precioProm.toFixed(2));
}

function obtenerMenor(listaProductos) {
    let listaOrdenada = _.orderBy(listaProductos, ['precio'], ['asc']);
    return listaOrdenada[0];
}

function obtenerMayor(listaProductos) {
    let mayor = _.maxBy(listaProductos, 'precio');
    return mayor;
}


console.log(obtenerNombre(productos));
console.log(calcularPrecioTotal(productos));
console.log(calcularPrecioPromedio(productos));
console.log(obtenerMenor(productos));
console.log(obtenerMayor(productos));