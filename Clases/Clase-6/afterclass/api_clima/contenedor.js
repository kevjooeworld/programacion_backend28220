const fs = require('fs');

class Contenedor{
    rutaArchivo;
    listaCiudades;

    constructor(){
        this.rutaArchivo = './ciudades.data.txt';
    }

    /* 
        Consultar los datos en la ruta y retornar una coleccion de objetos.
        Este metodo es sync porque dependo de los datos de respuesta 
    */
    obtenerCiudades(){
        try {
            let contenido = fs.readFileSync(this.rutaArchivo, 'utf-8');
            if (contenido) {
                this.listaCiudades = JSON.parse(contenido);
            } else {
                this.listaCiudades = [];
            }

            return this.listaCiudades;
        } catch (error) {
            return [];
        }
    }

    /* 
        Agregar los datos de una ciudad y guardarlo.
    */
    agregarCiudad(nombre, pais, temperaturas){
        let idCiudad = 0;

        let listaObjetos = this.obtenerCiudades();

        if (listaObjetos.length !== 0) {
            idCiudad = listaObjetos.length + 1;
        } else {
            idCiudad = 1;
        }

        let objeto = {
            id: idCiudad,
            nombre: nombre,
            pais: pais,
            temperaturas: temperaturas
        }
        
        listaObjetos.push(objeto);

        fs.writeFileSync(this.rutaArchivo, JSON.stringify(listaObjetos, null, 2));
        return idCiudad;
    }
}

module.exports = Contenedor;
