const express = require('express');
const bodyParser = require('body-parser');

const PORT = 5000;

const Contenedor = require('./contenedor');
const objContenedor = new Contenedor();

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));

app.get('/ciudades', (req, res) => {
    let listaCiudades = objContenedor.obtenerCiudades();
    res.json(listaCiudades);
});

app.get('/ciudades/:id', (req, res) => {
    let parId = req.params.id;
    res.send(`Obtiene los datos de una ciudad: ${parId}`);
});

app.post('/ciudad', (req, res) => {
    let parNombre = req.body.nombre;
    let parPais = req.body.pais;
    
    //En cuanto a las temperaturas se debe crear un endpoint para agregar las temperaturas a una ciudad y no enviar valores por defecto

    let idCiudad = objContenedor.agregarCiudad(parNombre, parPais, [{horaI: '00:00:00', horaF: '05:00:00', temperatura: 15}, {horaI: '05:00:00', horaF: '10:00:00', temperatura: 18}])
    
    res.json({code:200, data:`Ciudad Agregada de forma satisfactoria id: ${idCiudad}!`});
});

app.put('/ciudad', (req, res) => {
    res.send('Modifica los datos de una ciudad');
});

const server = app.listen(PORT, () => {
    console.log(`Servidor http escuchando en el puerto: ${server.address().port}`)
});

server.on("error", error => console.log(`Error en servidor ${error}`));
