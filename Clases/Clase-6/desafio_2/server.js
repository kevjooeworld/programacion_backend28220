const express = require('express');
const moment = require('moment');

const app = express();

const PORT = 4445;

app.get('/', (req, res) =>{
    res.send('<h1 style="color:blue;">Bienvenidos al servidor express </h1>');
});

let visitas = 0;
app.get('/visitas', (req, res) =>{
    
    visitas++;
    res.send(`<h1 style="color:blue;">${visitas}</h1>`);
});

app.get('/fyp', (req, res) =>{
    let fecha = moment(new Date()).format("DD/MM/YYYY HH:mm:ss");
    res.send(`<h1 style="color:green;">${fecha}</h1>`);
});

const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
});

server.on("error", error => {
    console.error(error);
});