class Persona{
    constructor (nombre, fecha, direccion) {
        this.nombre = nombre;
        this.fechaNacimiento = fecha;
        this.direccion = direccion;
   }

   mostrarNombre() {
    console.log(this.nombre);     
   } 
}

let persona1 = new Persona('Jorge', '22/12/2021', 'Buenos Aires');
persona1.mostrarNombre();

//console.log(persona1);