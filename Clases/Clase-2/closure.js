function crearConfiguracion(dispositivo) {
    return function (version) {
        console.log(`El ${dispositivo} ha inicializado con la version: ${version}`);
    }
}

let fxConfiguracion = crearConfiguracion('Samsung Galaxy S21');

fxConfiguracion(' Ultra');
console.log(fxConfiguracion);
console.log(fxConfiguracion());

//podes tener una maquina que arma sanguches y que se pueden personalizar:

function armarSanguche(deQue){
return function(adicionales){
		console.log(`el sanguche es de ${deQue} con ${adicionales}`)
	}
}

let sangucheDeCarne = armarSanguche('carne')
let sangucheDePollo = armarSanguche('pollo')


sangucheDeCarne('lechuga y tomate')
sangucheDePollo('jamon y queso')

