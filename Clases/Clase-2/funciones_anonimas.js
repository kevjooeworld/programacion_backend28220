// Funcion variable
let foo = function( ){ console.log('cuerpo de la fx')};
foo();

//Funcion autollamda
(function () {
    console.log("Realiza la configuracion inicial!");
})();

// Autollamada con valor default
((conf="default") => 
{
    console.log("Realiza la configuracion inicial: " + conf);
})();

//// Autollamada con valor asignado
((conf="default") => 
{
    console.log("Realiza la configuracion inicial: " + conf);
})('configuracion adicional');