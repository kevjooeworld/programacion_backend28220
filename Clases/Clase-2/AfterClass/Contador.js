class Contador{
    responsable;
    cuentaIndividual;
    static cuentaGlobal = 0;

    constructor(responsable){
        this.responsable = responsable;
        this.cuentaIndividual = 0;
    }

    obtenerResponsable(){
        return this.responsable;
    }

    obtenerCuentaIndividual(){
        return this.cuentaIndividual;
    }

    obtenerCuentaGlobal(){
        return Contador.cuentaGlobal;
    }

    contar(){
        this.cuentaIndividual++;
        Contador.cuentaGlobal++;
    }
}

let contador1 = new Contador('Sonic');
let contador2 = new Contador('kirby');
let contador3 = new Contador('Leon');

contador1.contar();
contador1.contar();
console.log(contador1.obtenerResponsable());
console.log(contador1.obtenerCuentaIndividual());
console.log(contador1.obtenerCuentaGlobal());

contador2.contar();
console.log(contador2.obtenerResponsable());
console.log(contador2.obtenerCuentaIndividual());
console.log(contador2.obtenerCuentaGlobal());

contador3.contar();
contador3.contar();
contador3.contar();
console.log(contador3.obtenerResponsable());
console.log(contador3.obtenerCuentaIndividual());
console.log(contador3.obtenerCuentaGlobal());
