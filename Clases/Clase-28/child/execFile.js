const { exec, execFile } = require('child_process');

execFile(__dirname + '/ejemplo.bat', (err, stdout, stderr) => {
    if (err) {
        console.log('error: ', err)
    }

    if (stderr) {
        console.log('error: ', stderr)
    }

    console.log('stdout', stdout)
});