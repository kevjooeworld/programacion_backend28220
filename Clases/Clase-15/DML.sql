-- DQL (Data Query Language):
-- DQL statements are used for performing queries on the data within schema objects. The purpose of the DQL Command is to get some schema relation based on the query passed to it. We can define DQL as follows it is a component of SQL statement that allows getting data from the database and imposing order upon it. It includes the SELECT statement. This command allows getting the data out of the database to perform operations with it. When a SELECT is fired against a table or tables the result is compiled into a further temporary table, which is displayed or perhaps received by the program i.e. a front-end.

-- List of DQL: 

-- SELECT: It is used to retrieve data from the database.
-- DML(Data Manipulation Language): 
-- The SQL commands that deals with the manipulation of data present in the database belong to DML or Data Manipulation Language and this includes most of the SQL statements. It is the component of the SQL statement that controls access to data and to the database. Basically, DCL statements are grouped with DML statements.

-- List of DML commands: 

-- INSERT : It is used to insert data into a table.
-- UPDATE: It is used to update existing data within a table.
-- DELETE : It is used to delete records from a database table.
-- LOCK: Table control concurrency.
-- CALL: Call a PL/SQL or JAVA subprogram.
-- EXPLAIN PLAN: It describes the access path to data.

-- /=====================================================================================/

INSERT INTO mibase.usuarios
(`nombre`, `apellido`, `edad`, `email`)
VALUES
('Juan', 'Perez', 23, 'jp@gmail.com'),
('Pedro', 'Mei', 21, 'pm@gmail.com'),
('Juana', 'Suarez', 25, 'js@gmail.com');

-- Consultar registros
select * from mibase.usuarios;

-- Eliminar registros
delete from mibase.usuarios where id = 2;

-- Actualizar registros
update mibase.usuarios set edad = 24 where id = 1;