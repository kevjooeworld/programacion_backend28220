/* ---------------------- Modulos ----------------------*/
const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const path = require('path');
/* ---------------------- Instancia de express ----------------------*/
const app = express();

/* ---------------------- Middlewares ---------------------- */
app.use(express.static('public'));
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }));

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads')
    },
    filename: (req, file, cb) => {
        cb(null, `${Date.now()} - ${file.originalname }`)
    }
});

const upload = multer({storage: storage});

/* ---------------------- Rutas ----------------------*/
app.post('/guardar', upload.single('miArchivo'), (req, res, next)=>{
    const file = req.file
    if (!file) {
        const err = new Error('Favor agregar archivo');
        return next(err);
    } else {
        res.status(200).json({msg:'Archivo subido satisfactoriamente'});   
    }
});

app.get('/formulario', (req, res)=>{
    let directorioHome = path.join(__dirname, './public/formulario.html'); 
    console.log(`Estamos ubicados en: ${directorioHome}`);
    res.sendFile(directorioHome);
    //res.send(__dirname)
});



app.post('subirFoto', (req, res)=>{

});


/* ---------------------- Servidor ----------------------*/
const PORT = 7447;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
})