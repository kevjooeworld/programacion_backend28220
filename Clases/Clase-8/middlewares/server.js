/* ---------------------- Modulos ----------------------*/
const express = require('express');
const morgan = require('morgan');

const router = require('./routes');
/* ---------------------- Instancia de express ----------------------*/
const app = express();
/* ---------------------- Middlewares ----------------------*/
app.use(express.static('public'), (req, res, next)=>{
    console.log('midd de otros modulos')
    next()
})

app.use((req, res, next)=>{
    console.log(`Midd de guardado de logs de entrada: ${Date.now()}`);
    next();
});

app.use((req, res, next)=>{
    console.log(`Midd de aplciacion 2: ${Date.now()}`);
    next();
});



app.use('/api', router);

const PORT = 8081;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
})
/* ---------------------- Servidor ----------------------*/
