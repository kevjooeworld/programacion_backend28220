const express = require('express');
const { append } = require('vary');
const router = express.Router();

/* ---------------------- Middlewares ----------------------*/

router.use((req, res, next)=>{
    console.log(`Consultar disponibilidad antes recursos de ejecutar la ruta: ${Date.now()}`);
    next();
});

function middlewareRuta1(req, res, next) {
    console.log(`Entra a ejecutar midd 1 la ruta: ${Date.now()}`);
    next();
}

function middlewareRuta2(req, res, next) {
    console.log(`Entra a ejecutar midd 2 la ruta: ${Date.now()}`);
    next();
}

router.get('/holamundo', middlewareRuta1, middlewareRuta2, (req, res, next) => {
    res.status(200).json({msg:'Te saluda Coder House! desde un router'})
})

module.exports = router;