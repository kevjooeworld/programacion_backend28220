/* ---------------------- Modulos ----------------------*/
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const routerPersonas = require('./rutas/personas.routes');//Segmento de rutas 1
const routerMascotas = require('./rutas/mascotas.routes');//Segmento de rutas 2
/*Tanto el segmento 1 como el segmento 2 luego se agregan a mi aplicacion de express (app)*/
/* ---------------------- Instancia de express ----------------------*/
const app = express();

/* ---------------------- Middlewares ---------------------- */
app.use(morgan('tiny'));
routerPersonas.use(express.json());
routerMascotas.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

/* ---------------------- Rutas ----------------------*/
/*Agregamos routers a la app*/
app.use('/api/personas', routerPersonas);
app.use('/api/mascotas', routerMascotas);

/* ---------------------- Servidor ----------------------*/
const PORT = 7071;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
})