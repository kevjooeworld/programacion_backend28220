const express = require('express');
const routerPersonas = express.Router();//Segmento de rutas 2

/*Personas*/
const personas = [];
routerPersonas.get('/', (req, res)=>{
    res.status(200).json(personas);
})

routerPersonas.post('/', (req, res)=>{
    console.log(req.body);
    personas.push(req.body);
    res.status(200).json({msg: 'Agregado!', data:personas});
})

module.exports = routerPersonas;
