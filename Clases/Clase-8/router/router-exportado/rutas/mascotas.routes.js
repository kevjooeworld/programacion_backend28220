const express = require('express');
const routerMascotas = express.Router();//Segmento de rutas 2

/*Mascotas*/
const mascotas = [];
routerMascotas.get('/', (req, res)=>{
    res.status(200).json(mascotas);
});

routerMascotas.get('/darPatita', (req, res)=>{
    res.status(200).json({msg:'Te da la patita!'});
});

routerMascotas.post('/', (req, res)=>{
    let mascota = req.body;
    mascotas.push(mascota);
    console.log('Post mascotas')
    res.status(200).json({msg: 'Agregado!', data:mascota});
});

module.exports = routerMascotas;