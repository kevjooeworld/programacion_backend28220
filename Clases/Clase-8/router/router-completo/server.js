/* ---------------------- Modulos ----------------------*/
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const routerPersonas = express.Router();//Segmento de rutas 1
const routerMascotas = express.Router();//Segmento de rutas 2
/*Tanto el segmento 1 como el segmento 2 luego se agregan a mi aplicacion de express (app)*/
/* ---------------------- Instancia de express ----------------------*/
const app = express();

/* ---------------------- Middlewares ---------------------- */
app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({ extended: true }));
routerPersonas.use(express.json());
routerMascotas.use(express.json());
app.use('/api/personas', routerPersonas);
app.use('/api/mascotas', routerMascotas);

/* ---------------------- Rutas ----------------------*/
/*Personas*/
const personas = [];
routerPersonas.get('/', (req, res)=>{
    res.status(200).json(personas);
})

routerPersonas.post('/', (req, res)=>{
    console.log(req.body);
    personas.push(req.body);
    res.status(200).json({msg: 'Agregado!', data:personas});
})

/*Mascotas*/
const mascotas = [];
routerMascotas.get('/', (req, res)=>{
    res.status(200).json(mascotas);
})

routerMascotas.post('/', (req, res)=>{
    let mascota = req.body;
    mascotas.push(mascota);
    res.status(200).json({msg: 'Agregado!', data:mascota});
})

/*Agregamos routers a la app*/
app.use('/api/personas', routerPersonas);
app.use('/api/mascotas', routerMascotas);


/* ---------------------- Servidor ----------------------*/
const PORT = 7071;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
})