/* ---------------------- Modulos ----------------------*/
const express = require('express');
const routerRutas1 = express.Router();
const routerRutas2 = express.Router();

/* ---------------------- Instancia de express ----------------------*/
const app = express();

/* ---------------------- Middlewares ---------------------- */

/* ---------------------- Rutas ----------------------*/
/* Router 1*/

/* Endpoint /api/router1/recurso1   */
routerRutas1.get('/recurso1', (req, res)=>{
    res.send('Consumo ruta /recurso1 en router1')
});

routerRutas1.get('/', (req, res)=>{
    res.send('Consumo ruta base de router /api en router1')
});

/* Router 2*/
routerRutas2.get('/recurso2', (req, res)=>{
    res.send('Consumo ruta /recurso2 en router2')
});

routerRutas2.get('/', (req, res)=>{
    res.send('Consumo ruta base de router /api en router2')
});

app.use('/api/router1', routerRutas1);
app.use('/api/router2', routerRutas2);

/* ---------------------- Servidor ----------------------*/
const PORT = 8081;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
})