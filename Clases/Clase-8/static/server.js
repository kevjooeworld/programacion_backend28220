/* ---------------------- Modulos ----------------------*/
const express = require('express');
const bodyParser = require('body-parser');
/* ---------------------- Instancia de express ----------------------*/
const app = express();

/* ---------------------- Middlewares ---------------------- */
app.use(express.static('public'));
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }));

/* ---------------------- Rutas ----------------------*/
app.post('/guardar', (req, res)=>{
    let mascota = {
        nombre: req.body.nombre,
        edad: req.body.edad,
        raza: req.body.raza
    }
    console.log(req.body);
    res.status(200).json({msg:'mascota recibida', mascota: mascota});
});

app.post('subirFoto', (req, res)=>{

});


/* ---------------------- Servidor ----------------------*/
const PORT = 7272;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
})