/* ---------------------- Modulos ----------------------*/
import express from 'express';

import routerOperaciones from './src/routes/operaciones.routes.js';

/* ---------------------- Instancia de express ----------------------*/
const app = express();

/* ---------------------- Middlewares ---------------------- */
routerOperaciones.use(express.json());
app.use(express.json());

/* ---------------------- Rutas ----------------------*/
app.use('/api/operaciones', routerOperaciones);

/*Agregamos routers a la app*/
app.get('/', (req, res)=>{
    res.send('Proyecto iniciado!')
})

/* ---------------------- Servidor ----------------------*/
const PORT = process.env.port || 3000;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
});