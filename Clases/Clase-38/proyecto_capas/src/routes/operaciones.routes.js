import express from 'express';
import { division, multiplicacion, recuperarHistorico, resta, suma } from '../controllers/operaciones.controller.js';

const routerOperaciones = express.Router();

routerOperaciones.get('/historico', async (req, res)=>{
    res.status(201).send(await recuperarHistorico());
});

routerOperaciones.post('/suma', async (req, res)=>{
    res.status(201).send(
        await suma(
            {tipo: "suma(+)", ...req.body}
        )
    );
});

routerOperaciones.post('/resta', async (req, res)=>{
    res.status(201).send(
        await resta(
            {tipo: "resta(-)", ...req.body}
        )
    );
});

routerOperaciones.post('/multiplicacion', async (req, res)=>{
    res.status(201).send(
        await multiplicacion(
            {tipo: "multiplicacion(*)", ...req.body}
        )
    );
});

routerOperaciones.post('/division', async (req, res)=>{
    res.status(201).send(
        await division(
            {tipo: "division(/)", ...req.body}
        )
    );
});

export default routerOperaciones;