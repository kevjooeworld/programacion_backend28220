import OperacionesDAOArchivos from "../service/OperacionesDAOArchivos.js";
import OperacionesDAOMemoria from "../service/OperacionesDAOMemoria.js";
import OperacionesDAOMongoDb from "../service/OperacionesDAOMongodb.js";
import dotenv from 'dotenv';
dotenv.config();

let DAO = {};

const persistencia = process.env.PERSIS;

if (persistencia === 'ARCH'){
    DAO = new OperacionesDAOArchivos();
} else if (persistencia === 'MONGODB'){
    DAO = new OperacionesDAOMongoDb();
} else {
    DAO = new OperacionesDAOMemoria();
}


async function suma(req) {
    const operacion = {
        tipo: req.tipo, 
        parametros: req.parametros || []
    }

    try {
        operacion.resultado = operacion.parametros[0] + operacion.parametros[1];
        return await DAO.guardar(operacion); 
    } catch (error) {
        return {
            code: '01',
            des: 'Error al sumar'
        }
    }
}

async function resta(req) {
    const operacion = {
        tipo: req.tipo, 
        parametros: req.parametros || []
    }

    try {
        operacion.resultado = operacion.parametros[0] - operacion.parametros[1]
        return await DAO.guardar(operacion); 
    } catch (error) {
        return {
            code: '01',
            des: 'Error al restar'
        }
    }
}

async function multiplicacion(req) {
    const operacion = {
        tipo: req.tipo, 
        parametros: req.parametros || []
    }

    try {
        operacion.resultado = operacion.parametros[0] * operacion.parametros[1]
        return await DAO.guardar(operacion); 
    } catch (error) {
        return {
            code: '01',
            des: 'Error al multiplicar'
        }
    }
}

async function division(req) {
    const operacion = {
        tipo: req.tipo, 
        parametros: req.parametros || []
    }

    try {
        operacion.resultado = operacion.parametros[0] / operacion.parametros[1]
        return await DAO.guardar(operacion); 
    } catch (error) {
        return {
            code: '01',
            des: 'Error al dividir'
        }
    }
}

async function recuperarHistorico() {
    return await DAO.listarAll();
}

export {suma, resta, multiplicacion, division, recuperarHistorico};