import ContenedorMemoria from '../containers/Contenedor.memoria.js';

class OperacionesDAOMemoria  extends ContenedorMemoria {
    guardar(elemento){
        const item = {
            ...elemento,
            tiempo: new Date()
        }

        return super.guardar(item);
    }
}

export default OperacionesDAOMemoria;