import ContenedorArchivo from '../containers/Contenedor.archivos.js';

class OperacionesDAOArchivos  extends ContenedorArchivo {
    constructor(){
        super('operaciones.json');
    }
    
    guardar(elemento){
        const item = {
            ...elemento,
            tiempo: new Date()
        }

        return super.guardar(item);
    }
}

export default OperacionesDAOArchivos;