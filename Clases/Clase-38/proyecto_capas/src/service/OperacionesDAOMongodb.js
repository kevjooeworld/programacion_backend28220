import ContenedorMongoDb from "../containers/Contenedor.mongodb.js";

class OperacionesDAOMongoDb extends ContenedorMongoDb {

    constructor() {
        super('historico', {
            tipo: { type: String, required: true },
            parametros: { type: Array, required: true },
            resultado: { type: Number, required: true },
        });
    }
}

export default OperacionesDAOMongoDb;
