import { promises as fs } from 'fs'
import config from '../utils/config.js'

class ContenedorArchivo {
    constructor(ruta) {
        this.ruta = `${config.fileSystem.path}/${ruta}`;
    }

    async listarAll() {
        console.log(this.ruta);
        try {
            const objs = await fs.readFile(this.ruta, 'utf-8')

            console.log(JSON.parse(objs));
            return JSON.parse(objs)
        } catch (error) {
            return []
        }
    }
    
    async guardar(obj) {
        const objs = await this.listarAll()

        let newId
        if (objs.length == 0) {
            newId = 1
        } else {
            newId = objs[objs.length - 1].id + 1
        }

        const newObj = { ...obj, id: newId }
        objs.push(newObj)

        try {
            await fs.writeFile(this.ruta, JSON.stringify(objs, null, 2))
            return newObj
        } catch (error) {
            throw new Error(`Error al guardar: ${error}`)
        }
    }
}

export default ContenedorArchivo;