class ContenedorMemoria {
    constructor(){
        this.elementos = [];
    }

    listarAll(){
        return [...this.elementos];
    }

    guardar(elem) {

        let newId
        if (this.elementos.length == 0) {
            newId = 1
        } else {
            newId = this.elementos[this.elementos.length - 1].id + 1
        }

        const newElem = { ...elem, id: newId }
        this.elementos.push(newElem)
        return newElem
    }
}

export default ContenedorMemoria;