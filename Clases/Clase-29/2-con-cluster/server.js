const express = require('express');
const cluster = require('cluster');
const numCPUs = require('os').cpus().length

const app = express();

if (cluster.isMaster) {
    console.log('num cpus: ', numCPUs);

    for (let index = 0; index < numCPUs; index++) {
        cluster.fork();
    }

    cluster.on('exit', worker=>{
        console.log(`Worker ${process.pid} finalizo ${new Date().toLocaleString()}`);
        cluster.fork()
    });

} else {
    // console.log('Proceso hijo')
    const PORT = parseInt(process.argv[2]) || 8080;

    app.get('/', (req, res)=>{
        res.send(`Servidor express en ${PORT} - PID ${process.pid} - ${new Date().toLocaleString()}`)
    })
    
    app.listen(PORT, err => {
        if (!err) console.log(`Servidor express escuchando en el puerto ${PORT} - PID WORKER ${process.pid}`)
    });
}