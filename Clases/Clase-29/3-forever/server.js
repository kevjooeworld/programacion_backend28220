/*
    Comandos de la clase

    Inicia los server por puertos
    $ forever start server.js 8081
    $ forever start server.js 8082
    $ forever start server.js 8083

    Se muestran los datos de los procesos en ejecucion
    $ forever list
    
    Lista todos los procesos de node.js activos
    $ tasklist /fi "imagename eq node.exe"

    Para comprobar la propiedad de monitor de forever, ejecutamos cierre del pgm a nivel de SO 
    $ taskkill /pid <id (2do num)> /f
    
    Para finalizar el programa y su monitoreo
    $ forever stop <id (2do num)>

    Para finalizar todos los procesos 
    $ forever stopall
*/

const express = require('express')

const app = express()

const PORT = parseInt(process.argv[2]) || 8080;

app.get('/', (req, res)=>{
    res.send(`Servidor express en ${PORT} - PID ${process.pid} - ${new Date().toLocaleString()}`)
})

app.listen(PORT, err => {
    if (!err) console.log(`Servidor express escuchando en el puerto ${PORT} - PID WORKER ${process.pid}`)
});