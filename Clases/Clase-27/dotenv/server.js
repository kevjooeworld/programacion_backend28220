import express  from "express";
import dotenv from "dotenv";
dotenv.config();

const app = express()

const PORT = process.env.PUERTO || 3000
const HOST = process.env.HOST || '127.0.0.1'

app.listen(PORT, ()=>{
    console.log(`Servidor iniciado en ${HOST}:${PORT} para ${process.env.CODERGROUP}`);
});

// PUERTO=3001
// HOST=127.0.0.1
// CODERGROUP=BACKEND28220PLUS