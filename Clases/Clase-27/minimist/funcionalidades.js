import minimist from "minimist";

const args = minimist(process.argv.slice(2));
console.log(args);

/*============== [Simulando entradas por CLI] ==============*/
console.log(minimist([1, 2, 3, 4, 5, 6]));
// { _: [ 1, 2, 3, 4, 5, 6 ] }

console.log(minimist(['1', '2', '10.45.23', {a: 1, b: 2}]));

/*--------- Asignando parametros ---------*/
console.log(minimist(['-a', 1, '2','--b1', '10.45.23','--cObjeto', {a: 1, b: 2}]));

/*--------- Asignando Parametros como banderas/indicadores ---------*/
let args1 = minimist(['-a', 1,'--b1', '10.45.23','--bandera1']);
console.log(args1._);
console.log(args1.a);

/* --------- asignando alias a los parametros*/
let options = {alias: {a: 'parametro1', b: 'parametro2'}};
console.log(minimist(['-a', '172.10.128.1', '--parametro2', 'RUTEAR'], options));

options = {alias: {v:'version'}}
console.log(minimist(['-v'], options));
console.log(minimist(['--version'], options));

/* --------- asignando valores default*/
options = {default: {nombre: 'Bandalos', apellido: 'Chinos'}};
console.log(minimist(['valor-sin-param', '--bandera1', '-a', 1,'--nombre', 'Bandadalos'], options));


