import dotenv from "dotenv";
dotenv.config();

export const MONGODBURL =  process.env.MONGODBURL;
export const HOLA =  process.env.HOLA || 'DefaultValueHOLA';
