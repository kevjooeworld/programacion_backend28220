import dotenv from "dotenv"
dotenv.config()

const config = {
    FACEBOOK_APP_ID: process.env.FACEBOOK_APP_ID, 
    FACEBOOK_APP_SECRET: process.env.FACEBOOK_APP_SECRET, 
    PORT: process.env.PORT || 3000
}

export default config;