const mostrarLetras = (palabra, tiempoDemora, callback) => {
    let contador = 0;
    let iteracion = setInterval(() => {
        console.log(palabra.charAt(contador));
        contador += 1;
        if (contador === palabra.length){
            clearInterval(iteracion);
            callback();
        }
    }, tiempoDemora);
    
}

mostrarLetras('¡Hola!', 0, () => console.log('terminé'));
mostrarLetras('******', 250, () => console.log('terminé'));
mostrarLetras('@@@@@@', 500, () => console.log('terminé'));